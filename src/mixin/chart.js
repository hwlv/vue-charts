const initEchart = {
  data () {
    return {
      myChart: null
    }
  },
  methods: {
    init (id, option) {
      this.myChart = this.$echarts.init(document.getElementById(id))
      this.myChart.setOption(option)
    }
  }
}

export default initEchart
