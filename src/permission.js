import router from './router'
import ScrollPosition from '@/lib/scroll-position.js'

router.beforeEach(async (to, from, next) => {
  ScrollPosition.save(from.path)
  console.log(ScrollPosition.cache())
  next()
})
