import Vue from 'vue'
import Router from 'vue-router'
import Animate from '@/views/Animate.vue'
import Layout from '@/layout'

Vue.use(Router)

export const constantRoutes = [
  {
    path: '/',
    component: Layout,
    redirect: '/home',
    children: [
      {
        path: 'home',
        component: () => import('@/views/Home'),
        name: 'Dashboard',
        meta: { title: 'Dashboard', icon: 'dashboard', affix: true }
      }
    ]
  },
  {
    path: '/about',
    title: 'about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
  },
  {
    path: '/animate',
    title: 'animate',
    name: 'animate',
    component: Animate
  },
  {
    path: '/chart',
    name: 'chart',
    title: 'chart',
    component: Layout,
    children: [
      {
        path: '/chart/vuechart',
        title: 'vuechart',
        name: 'about',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ './views/Vuechart.vue')
      },
      {
        path: '/chart/basic',
        title: 'basic',
        component: () => import('@/views/chart/Basic'),
        name: 'Basic',
        meta: { title: 'Basic' }
      },
      {
        path: '/chart/strip',
        title: 'strip',
        component: () => import('@/views/chart/Strip'),
        name: 'Strip',
        meta: { title: 'Strip' }
      },
      {
        path: '/chart/grid',
        title: 'grid',
        name: 'grid',
        component: () => import('@/views/chart/Grid'),
        meta: { title: 'grid' }
      }
    ]
  }
]

export default new Router({
  mode: 'history',
  routes: constantRoutes
  // scrollBehavior (to, from, savedPosition) {
  //   console.log(savedPosition)
  //   // return 期望滚动到哪个的位置
  //   // if (savedPosition) {
  //   //   return savedPosition
  //   // } else {
  //   //   return { x: 0, y: 0 }
  //   // }
  // }
})
