
export function filterAsyncRoutes (routes) {
       const res = []
  routes.forEach(route => {
    const tmp = { }
    tmp.title = route.title
    if (tmp.children) {
      tmp.children = filterAsyncRoutes(tmp.children)
    }
    res.push(tmp)
  })

  return res
}
